from tkinter import *
import tkinter.messagebox as tkMessageBox
from PIL import Image, ImageTk
import socket, threading, sys, traceback, os

from RtpPacket import RtpPacket

CACHE_FILE_NAME = "cache-"
CACHE_FILE_EXT = ".jpg"

class Client:
	# State
	INIT = 0
	READY = 1
	PLAYING = 2
	SWITCH = 3
	state = INIT

	# RequestSent
	DESCRIBE = 0
	GETLISTVIDEO = 1
	SETUP = 2
	PLAY = 3
	PAUSE = 4
	TEARDOWN = 5
	BACKWARD = 6
	FORWARD = 7	

	# Set state close gui
	NOT_EXIT = 1

	# Initiation..
	def __init__(self, master, serveraddr, serverport, rtpport, filename):
		self.master = master
		self.master.protocol("WM_DELETE_WINDOW", self.handler)
		self.serverAddr = serveraddr
		self.serverPort = int(serverport)
		self.rtpPort = int(rtpport)
		self.fileName = filename
		self.rtspSeq = 0
		self.sessionId = 0
		self.requestSent = -1
		self.teardownAcked = 0
		self.byteDataCurr = 0
		self.frameNbr = 0
		self.packetSent = 0
		self.packetRecv = 0
		self.hasGetList = 0
		self.createWidgets()
		self.connectToServer()
		self.setupMovie()
		
	# THIS GUI IS JUST FOR REFERENCE ONLY, STUDENTS HAVE TO CREATE THEIR OWN GUI 	
	def createWidgets(self):
		"""Build GUI."""

		# Create a option menu to select file video
		self.value_inside = StringVar(self.master)
		self.value_inside.set("Select Video")
		self.optionName = OptionMenu(self.master, self.value_inside, "")
		self.optionName.grid(row=0,column=0, padx=0, pady=0)

		# Create a label to guide to choose filename
		self.nameLabel = Label(self.master, height=5, anchor=W, justify=LEFT)
		self.nameLabel.grid(row=0, column=1, padx=0, pady=0)
		self.nameLabel.configure(text="<-- Switch Video")
		
		# Create a label to display the statistics
		self.statNameLabel = Label(self.master, height=5, anchor=E, justify=RIGHT)
		self.statNameLabel.grid(row=0, column=3, padx=0, pady=0)
		self.statNameLabel.configure(text="Data Size:\nPacket Loss:\nPacket Loss Rate:\nVideo Data Rate:\nFPS:")
		self.statLabel = Label(self.master, height=5, anchor=E, justify=RIGHT)
		self.statLabel.grid(row=0, column=4, padx=0, pady=0)
		self.statLabel.configure(text="{0:.2f} MB\n{1} packets/s\n{2:.2f}%\n{3} bytes/s\n{4}".format(os.path.getsize(self.fileName)/1024**2,0,0,0,0))

		# Create a label to display the movie
		self.label = Label(self.master, height=19)
		self.label.grid(row=1, column=0, columnspan=5, sticky=W+E+N+S, padx=6, pady=6) 

		# Create Backward button		
		self.back = Button(self.master, width=20, padx=3, pady=3)
		self.back["text"] = "Backward"
		self.back["command"] = self.backwardMovie
		self.back.grid(row=2, column=0, padx=2, pady=2)

		# Create Play and Pause button		
		self.start_pause = Button(self.master, width=20, padx=3, pady=3)
		self.start_pause["text"] = "Play"
		self.start_pause["command"] = self.playnpauseMovie
		self.start_pause.grid(row=2, column=1, padx=2, pady=2)
		
		# Create Fast Forward button		
		self.forw = Button(self.master, width=20, padx=3, pady=3)
		self.forw["text"] = "Fast Forward"
		self.forw["command"] = self.forwardMovie
		self.forw.grid(row=2, column=3, padx=2, pady=2)
		
		# Create Teardown button
		self.teardown = Button(self.master, width=20, padx=3, pady=3)
		self.teardown["text"] = "Stop"
		self.teardown["command"] =  self.exitClient
		self.teardown.grid(row=2, column=4, padx=2, pady=2)
	
	# Describe request
	def getDescribe(self):

		self.sendRtspRequest(self.DESCRIBE)
	
	# Setup request
	def setupMovie(self):
		"""Setup button handler."""
		#TODO
		if self.state == self.INIT:

			self.sendRtspRequest(self.SETUP)

			self.start_pause["text"] = "Play"

	# Teardown request
	def exitClient(self):
		"""Teardown button handler."""
		#TODO
		if self.state == self.SWITCH or self.requestSent == self.FORWARD or self.requestSent == self.BACKWARD or self.requestSent == self.PAUSE or self.state != self.READY:
			try:
				# Delete the cache image from video
				for file in os.listdir("./"):
					if file.endswith(CACHE_FILE_EXT):
						os.remove(file)
				# os.remove(CACHE_FILE_NAME + str(self.sessionId) + CACHE_FILE_EXT)
			except: pass
			self.sendRtspRequest(self.TEARDOWN)	

			self.statLabel.configure(text="{0:.2f} MB\n{1} packets/s\n{2:.2f}%\n{3} bytes/s\n{4}".format(os.path.getsize(self.fileName)/1024**2,0,0,0,0))

	# Play and pause request
	def playnpauseMovie(self):
		"""Play button handler."""
		#TODO
		if self.state == self.PLAYING:
			self.sendRtspRequest(self.PAUSE)
			self.start_pause["text"] = "Play"
		elif self.state == self.READY:
			self.start_pause["text"] = "Pause"
			# Create a new thread to listen for RTP packets
			threading.Thread(target=self.listenRtp, daemon=True).start()
			self.playEvent = threading.Event()
			self.playEvent.clear()
			self.sendRtspRequest(self.PLAY)

	# Backward request
	def backwardMovie(self):
		"""Backward button handler."""
		if self.requestSent != self.SETUP:
			self.sendRtspRequest(self.BACKWARD)
			if self.state == self.READY:
				self.listenRtp()

	# Fast forward request
	def forwardMovie(self):
		"""Fast Forward button handler."""
		if self.requestSent != self.SETUP:
			self.sendRtspRequest(self.FORWARD)
			if self.state == self.READY:
				self.listenRtp()

	# Calculate the statistics about the session
	def calcStat(self):
		while True:
			# --> Packet Loss = self.packetSent - self.packetRecv
			# --> Packet Loss Rate = (self.packetSent - self.packetRecv)*100 / self.packetSent
			# --> Video Data Rate = self.byteDataCurr - self.byteDataOld
			# --> FPS = self.frameNbr - self.oldFrame
			self.byteDataOld = self.byteDataCurr
			self.oldFrame = self.frameNbr

			threading.Event().wait(1)
			self.statLabel.configure(text="{0:.2f} MB\n{1} packets/s\n{2:.2f}%\n{3} bytes/s\n{4}".format(os.path.getsize(self.fileName)/1024**2, self.packetSent - self.packetRecv if self.packetSent > self.packetRecv else 0, (self.packetSent - self.packetRecv)*100 / self.packetSent if self.packetSent and self.packetSent > self.packetRecv else 0 , self.byteDataCurr - self.byteDataOld if self.byteDataCurr > self.byteDataOld else 0, self.frameNbr - self.oldFrame if self.frameNbr > self.oldFrame else 0))
			self.packetRecv = 0
			self.packetSent = 0
			
			if self.state == self.READY:
				break
	
	# Get list video name
	def getListVideoRequest(self):
		self.sendRtspRequest(self.GETLISTVIDEO)
	
	# Switch Video
	def switchVideo(self, choice):
		choice = self.value_inside.get()
		self.fileName = str('.\\' + choice)
		self.state = self.SWITCH
		self.exitClient()

	# Listen for RTP packets
	def listenRtp(self):		
		"""Listen for RTP packets."""
		#TODO
		while True:
			try:
				self.packetSent +=1
				data = self.rtpSocket.recv(20480)
				
				if data:
					self.packetRecv +=1
					self.byteDataCurr += len(data)
					rtpPacket = RtpPacket()
					rtpPacket.decode(data)
					currFrameNbr = rtpPacket.seqNum()

					print("Current Seq Num: " + str(currFrameNbr))

					if self.requestSent == self.BACKWARD:
						if self.frameNbr > 40:
							self.frameNbr -= 40
						else: self.frameNbr = 0

					# Discard the late packet	
					if currFrameNbr > self.frameNbr: 
						self.frameNbr = currFrameNbr
						self.updateMovie(self.writeFrame(rtpPacket.getPayload()))

			except:

				# Stop listening upon requesting PAUSE or TEARDOWN
				if self.playEvent.isSet(): 
					break

				# close the RTP socket
				if self.teardownAcked == 1:

					break

	def writeFrame(self, data):
		"""Write the received frame to a temp image file. Return the image file."""
		#TODO
		cachename = CACHE_FILE_NAME + str(self.sessionId) + CACHE_FILE_EXT
		file = open(cachename, "wb")
		file.write(data)
		file.close()

		return cachename

	def updateMovie(self, imageFile):
		"""Update the image file as video frame in the GUI."""
		#TODO
		if imageFile == 0:
			self.label.image = None
			return
		
		photo = ImageTk.PhotoImage(Image.open(imageFile))
		self.label.configure(image = photo, height=288) 
		self.label.image = photo

	def connectToServer(self):
		"""Connect to the Server. Start a new RTSP/TCP session."""
		#TODO
		self.rtspSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

		try:
			self.rtspSocket.connect((self.serverAddr, self.serverPort))
		except:
			tkMessageBox.showwarning('Connection Failed', 'Connection to \'%s\' failed.' %self.serverAddr)
			
	def sendRtspRequest(self, requestCode):
		"""Send RTSP request to the server."""	
		#-------------
		# TO COMPLETE
		#-------------
		# Setup request
		if requestCode == self.DESCRIBE:
			request = 'DESCRIBE ' + self.fileName + ' RTSP/1.0\nCSeq: 0'

		elif requestCode == self.GETLISTVIDEO:
			request = 'GETLISTVIDEO'
			self.requestSent = self.GETLISTVIDEO

		elif requestCode == self.SETUP and self.state == self.INIT:

			# Create a new thread to listen for RTSP packets
			threading.Thread(target=self.recvRtspReply, daemon=True).start()
			self.byteDataCurr = 0

			# Update RTSP sequence number.
			self.rtspSeq += 1

			# Write the RTSP request to be sent.
			request = 'SETUP ' + self.fileName + ' RTSP/1.0\nCSeq: ' + str(self.rtspSeq) + '\nTransport: RTP/UDP; client_port= ' + str(self.rtpPort)

			# Keep track of the sent request.
			self.requestSent = self.SETUP 

		# Play request
		elif requestCode == self.PLAY and self.state == self.READY:
			
			threading.Thread(target=self.calcStat, daemon=True).start()

			self.rtspSeq += 1

			request = 'PLAY ' + self.fileName + ' RTSP/1.0\nCSeq: ' + str(self.rtspSeq) + '\nSession: ' + str(self.sessionId)

			self.requestSent = self.PLAY

		# Backward request
		elif requestCode == self.BACKWARD:
			self.rtspSeq += 1

			request = 'BACKWARD ' + self.fileName + ' RTSP/1.0\nCSeq: ' + str(self.rtspSeq) + '\nSession: ' + str(self.sessionId)

			self.requestSent = self.BACKWARD

		# Fast forward request
		elif requestCode == self.FORWARD:
			self.rtspSeq += 1

			request = 'FORWARD ' + self.fileName + ' RTSP/1.0\nCSeq: ' + str(self.rtspSeq) + '\nSession: ' + str(self.sessionId)

			self.requestSent = self.FORWARD

		# Pause request
		elif requestCode == self.PAUSE and self.state == self.PLAYING:

			self.rtspSeq += 1

			request = 'PAUSE ' + self.fileName + ' RTSP/1.0\nCSeq: ' + str(self.rtspSeq) + '\nSession: ' + str(self.sessionId)

			self.requestSent = self.PAUSE


		# Teardown request
		elif requestCode == self.TEARDOWN and not self.state == self.INIT:

			self.rtspSeq += 1

			request = 'TEARDOWN ' + self.fileName + ' RTSP/1.0\nCSeq: ' + str(self.rtspSeq) + '\nSession: ' + str(self.sessionId) 

			self.requestSent = self.TEARDOWN

		else:
			return

		# Send the RTSP request using rtspSocket.
		self.rtspSocket.send(request.encode())

		if self.requestSent != self.GETLISTVIDEO: print ('\nData sent:\n' + request)
	
	def recvRtspReply(self):
		"""Receive RTSP reply from the server."""
		#TODO
		while True:
			reply = self.rtspSocket.recv(1024)

			if self.requestSent != self.GETLISTVIDEO: print ('\nData received:\n' + reply.decode())

			if reply: 

				self.parseRtspReply(reply.decode())
				
			# Close the RTSP socket upon requesting Teardown

			if self.requestSent == self.TEARDOWN:
				
				self.rtspSocket.shutdown(socket.SHUT_RDWR)

				self.rtspSocket.close()

				self.rtpSocket.shutdown(socket.SHUT_RDWR)

				self.rtpSocket.close()

				if self.NOT_EXIT:
					self.sessionId = 0
					self.frameNbr = 0
					self.teardownAcked = 0
					self.connectToServer()
					self.setupMovie()
					self.updateMovie(0)
				break

	def parseRtspReply(self, data):
		"""Parse the RTSP reply from the server."""
		#TODO
		lines = data.split('\n')

		seqNum = int(lines[1].split(' ')[1])

		# Process only if the server reply's sequence number is the same as the request's
		if seqNum == -1: 
			self.listVideo = lines[2].split(' ')[2:]
			self.value_inside.set(self.fileName)
			self.optionName = OptionMenu(self.master, self.value_inside, *self.listVideo, command = self.switchVideo)
			self.optionName.grid(row=0,column=0, padx=0, pady=0)
			return

		elif seqNum == self.rtspSeq:

			session = int(lines[2].split(' ')[1])

			# New RTSP session ID
			if self.sessionId == 0:

				self.sessionId = session

			

			# Process only if the session ID is the same
			if self.sessionId == session:

				if int(lines[0].split(' ')[1]) == 200: 

					if self.requestSent == self.SETUP:

						# Update RTSP state.
						self.state = self.READY

						# Open RTP port.
						self.openRtpPort()

						# self.getDescribe()
						if self.hasGetList == 0: 
							self.getListVideoRequest()
							self.hasGetList = 1

					elif self.requestSent == self.PLAY:

						self.state = self.PLAYING

					elif self.requestSent == self.PAUSE:

						self.state = self.READY

						# The play thread exits. A new thread is created on resume.
						self.playEvent.set()

					elif self.requestSent == self.TEARDOWN:

						self.state = self.INIT

						# Flag the teardownAcked to close the socket.
						self.teardownAcked = 1 

	def openRtpPort(self):
		"""Open RTP socket binded to a specified port."""
		#-------------
		# TO COMPLETE
		#-------------
		# Create a new datagram socket to receive RTP packets from the server
		# self.rtpSocket = ...
		self.rtpSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

		# Set the timeout value of the socket to 0.5sec
		self.rtpSocket.settimeout(0.5)

		try:
			# Bind the socket to the address using the RTP port given by the client user
			self.rtpSocket.bind(("", self.rtpPort))

		except:
			tkMessageBox.showwarning('Unable to Bind', 'Unable to bind PORT=%d' %self.rtpPort)

	def handler(self):
		"""Handler on explicitly closing the GUI window."""
		#TODO
		if self.state == self.PLAYING: self.playnpauseMovie()
		if tkMessageBox.askokcancel("Quit?", "Are you sure you want to quit?"):
			try:
				self.NOT_EXIT = 0
				self.sendRtspRequest(self.TEARDOWN)
				os.remove(CACHE_FILE_NAME + str(self.sessionId) + CACHE_FILE_EXT) # Delete the cache image from video
			except:
				pass
			self.master.destroy() # Close the gui window
			sys.exit()
		else: # When the user presses cancel, resume playing.
			if self.state == self.READY and self.requestSent == self.PAUSE: self.playnpauseMovie()