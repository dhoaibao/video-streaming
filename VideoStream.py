class VideoStream:
	def __init__(self, filename):
		self.filename = filename
		try:
			self.file = open(filename, 'rb')
		except:
			raise IOError
		self.frameNum = 0
		self.backwardData = []
		self.hasBack = 0
		self.ind = 0
		
	def nextFrame(self, back = 0, forw = 0):
		"""Get next frame."""
		num = 1
		if back and len(self.backwardData) > 0:
			if self.ind > 40:
				self.ind = self.ind - 40	#3
			else: self.ind = 0

			self.hasBack = 1

		if forw:
			self.ind += 40						#44
			if self.ind >= len(self.backwardData):
				self.hasBack = 0
				num = self.ind - self.frameNum
				self.ind = self.frameNum
			else:
				self.hasBack = 1

		if self.hasBack:
			data = self.backwardData[self.ind]	#43
			self.ind += 1						#43
			if self.ind == self.frameNum:
				self.hasBack = 0
			return data
		
		for x in range(num):
			data = self.file.read(5) # Get the framelength from the first 5 bits
			if data: 
				framelength = int(data)
								
				# Read the current frame
				data = self.file.read(framelength)
				self.frameNum += 1
				self.ind += 1
				self.backwardData.append(data)
		return data
		
	def frameNbr(self):
		"""Get frame number."""
		if self.hasBack: return self.ind
		return self.frameNum
	
	